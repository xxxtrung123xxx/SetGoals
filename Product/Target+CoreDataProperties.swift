//
//  Target+CoreDataProperties.swift
//  SetGoalsGoals
//
//  Created by Phạm Thành Trung on 31/03/2022.
//
//

import Foundation
import CoreData


extension Target {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Target> {
        return NSFetchRequest<Target>(entityName: "Target")
    }

    @NSManaged public var date: Date?
    @NSManaged public var target: String?

}

extension Target : Identifiable {

}

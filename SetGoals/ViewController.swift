//
//  ViewController.swift
//  SetGoals
//
//  Created by Phạm Thành Trung on 28/03/2022.
//

import UIKit
import CoreData

class ViewController: UIViewController{
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    private var models = [Target]()
    var  listDate:[String] = []
    var datePicker:UIDatePicker = UIDatePicker()
    let toolBar = UIToolbar()
    var goalText:UITextField?
    var dateText:UITextField?
    
    @IBAction func addButton(_ sender: Any) {
        testDatePicker()
    }
    
    @IBOutlet weak var targetTableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllItems()
        targetTableview.delegate = self
        targetTableview.dataSource = self
        title = "GetGoals"
    }
    
    func converDatetoString(date:Date)-> String{
        // Create Date Formatter
        let dateFormatter = DateFormatter()
        // Set Date Format
        dateFormatter.dateFormat = "dd/MM/yyyy"
        // Convert Date to String
        return dateFormatter.string(from: date)
    }
    
    func converStringtoDate(string: String)-> Date{
        // Create Date Formatter
        let dateFormatter = DateFormatter()
        // Set Date Format
        dateFormatter.dateFormat = "dd/MM/yyyy"
        // Convert String to Date
        return dateFormatter.date(from: string)!
    }
    
    func getAllItems(){
        do{
            models = try context.fetch(Target.fetchRequest())
            //sort array by date
            models.sort{
                $0.date! < $1.date!
            }
            
            DispatchQueue.main.async {
                self.targetTableview.reloadData()
            }
        }
        catch {
        }
    }
    
    func createItems(target:String,finishDate:Date){
        let newItem = Target(context: context)
        newItem.target = target
        newItem.date = finishDate
        do{
            try context.save()
            getAllItems()
        }catch{
            
        }
    }
    
    func deleteItems(item: Target){
        context.delete(item)
        do{
            try context.save()
            getAllItems()
        }catch{
        }
    }
    
    func updateItems(item: Target, newTarget: String, newDate: Date){
        item.target = newTarget
        item.date = newDate
        do{
            try context.save()
            getAllItems()
        }catch{
        }
    }
    
    func testDatePicker(){
        let datePicker = UIDatePicker()
        datePicker.preferredDatePickerStyle  = .wheels
        datePicker.datePickerMode = .date
        datePicker.frame = CGRect(x: 0, y: 40, width: 270, height: 200)
        
        let alert = UIAlertController(title: "Add goals", message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        alert.addTextField(configurationHandler:{ (textField) in
            textField.placeholder = "Enter your goal"
        })
        alert.view.addSubview(datePicker)
        
        let selectAction = UIAlertAction(title: "OK", style: .default,handler: {[weak self] action in
            guard let field = alert.textFields?.first, let text = field.text,!text.isEmpty else{
                return
            }
            self?.createItems(target: text, finishDate: datePicker.date)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(selectAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
}

extension ViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // fetchedResultsController.fetchedObjects!.count
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let model = models[indexPath.row]
        cell.textLabel?.text = model.target
        cell.detailTextLabel?.text = converDatetoString(date: model.date!)
        return cell
    }
}

extension ViewController:UITableViewDelegate{
    //detele cell when user swipe
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath)->UITableViewCell.EditingStyle {
        return .delete
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        targetTableview.beginUpdates()
        let model = models[indexPath.row]
        self.deleteItems(item: model)
        targetTableview.deleteRows(at: [indexPath], with: .automatic)
        targetTableview.endUpdates()
    }
    
    //show popup to edit when user click
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = models[indexPath.row]
        //edit goal
        let cellSelected1 = model.target
        let cellSelected2 = converDatetoString(date: model.date!)
        let alert = UIAlertController(title: "Edit", message: "Edit goals", preferredStyle: .alert)
        let update = UIAlertAction(title: "Update", style: .default) {[weak self] action in
            guard let field = alert.textFields?[0], let text = field.text,!text.isEmpty else{
                return
            }
            guard let field2 = alert.textFields?[1], let text2 = field2.text,!text2.isEmpty else{
                return
            }
            self?.updateItems(item:model, newTarget: text, newDate: self!.converStringtoDate(string: text2))
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancel)
        alert.addAction(update)
        
        //textField goal
        alert.addTextField{  (textfield) in
            self.goalText = textfield
            self.goalText?.placeholder = "Update your goal"
            self.goalText?.text = cellSelected1
        }
        
        //textField date
        alert.addTextField{ [self]  (textfield) in
            self.dateText = textfield
            self.dateText?.placeholder = "Update your date"
            self.dateText?.text = cellSelected2
        }
        self.present(alert, animated: true,completion: nil)
    }
}







